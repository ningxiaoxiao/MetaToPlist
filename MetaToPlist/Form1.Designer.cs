﻿namespace MetaToPlist
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox_plist = new System.Windows.Forms.CheckBox();
            this.checkBox_smallpic = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // checkBox_plist
            // 
            this.checkBox_plist.AutoSize = true;
            this.checkBox_plist.Location = new System.Drawing.Point(133, 31);
            this.checkBox_plist.Name = "checkBox_plist";
            this.checkBox_plist.Size = new System.Drawing.Size(78, 16);
            this.checkBox_plist.TabIndex = 0;
            this.checkBox_plist.Text = "生成Plist";
            this.checkBox_plist.UseVisualStyleBackColor = true;
            // 
            // checkBox_smallpic
            // 
            this.checkBox_smallpic.AutoSize = true;
            this.checkBox_smallpic.Location = new System.Drawing.Point(133, 9);
            this.checkBox_smallpic.Name = "checkBox_smallpic";
            this.checkBox_smallpic.Size = new System.Drawing.Size(72, 16);
            this.checkBox_smallpic.TabIndex = 1;
            this.checkBox_smallpic.Text = "生成小图";
            this.checkBox_smallpic.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 84);
            this.label1.TabIndex = 2;
            this.label1.Text = "本工具能把meta文本里的信息转成plist,用于有合成\r\n图却没有plist,要先在unity3d 4.3中进行切图.\r\n2014/2/11更新\r\n-使用C#重" +
    "写.支持多文件\r\n-可以过滤非meta文件\r\n-修复少一个图的bug\r\n-增加生成图片的选项\r\n";
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 38);
            this.label2.TabIndex = 3;
            this.label2.Text = "\r\n把meta文件拖到这里\r\n\r\n";
            this.label2.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.label2.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(12, 53);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(274, 76);
            this.listBox1.TabIndex = 5;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 231);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_smallpic);
            this.Controls.Add(this.checkBox_plist);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "metaToPlist";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox_plist;
        private System.Windows.Forms.CheckBox checkBox_smallpic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox1;
    }
}

