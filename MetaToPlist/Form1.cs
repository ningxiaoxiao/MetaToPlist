﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Text.RegularExpressions;

namespace MetaToPlist
{
    public partial class Form1 : Form
    {
        XmlDocument xmldoc;
        XmlNode xmlnode;
        XmlElement xmlelem;
        public Form1()
        {
            InitializeComponent();

        }
        void log(string e)
        {
            listBox1.Items.Insert(0, e);
        }
        private void button_ok_Click(object sender, EventArgs e)
        {
        }
        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Link;
        }
        /// <summary>
        /// 文件拖放操作
        /// </summary>
        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            Array aryFiles = ((Array)e.Data.GetData(DataFormats.FileDrop));

            for (int i = 0; i < aryFiles.Length; i++) //多文件支持
            {

                string filePath = aryFiles.GetValue(i).ToString();

                if (filePath.IndexOf("meta") == -1)
                {
                    Console.WriteLine("这个文件不合法");
                    break;
                }
                log("开始对" + Path.GetFileName(filePath) + "进行处理");
                //建立xml树
                xmldoc = new XmlDocument();
                //声明
                xmlnode = xmldoc.CreateNode(XmlNodeType.XmlDeclaration, "", "");
                xmldoc.AppendChild(xmlnode);
                //根
                xmlelem = xmldoc.CreateElement("plist");
                xmlelem.SetAttribute("version", "1.0");
                xmldoc.AppendChild(xmlelem);
                //加入前面的东西
                XmlNode root = xmldoc.SelectSingleNode("/plist");
                root.AppendChild(xmldoc.CreateElement("dict"));
                root = xmldoc.SelectSingleNode("/plist/dict");
                xmlelem = xmldoc.CreateElement("key");
                xmlelem.InnerText = "frames";
                root.AppendChild(xmlelem);
                //生成主体
                xmlelem = xmldoc.CreateElement("dict");

                #region 读入文件并解析文件

                //读入文本
                FileStream aFile = new FileStream(filePath, FileMode.Open);
                StreamReader sr = new StreamReader(aFile);
                sr.BaseStream.Seek(0, SeekOrigin.Begin);//移到文件头
                string meta = sr.ReadToEnd();//读出全部
                meta = meta.Replace(" ", null);//换掉空格

                //得到图片的高度.用于坐标第转换
                string picpath = Path.ChangeExtension(filePath, null);
                Image pic = Image.FromFile(picpath);


                string pattern = @"name:([\s\S]*?)rect:
serializedVersion:2
x:([\s\S]*?)
y:([\s\S]*?)
width:([\s\S]*?)
height:([\s\S]*?)
alignment:0
pivot:{x:([\s\S]*?),y:([\s\S]*?)}";
                #endregion
                //正则
                Regex r = new Regex(pattern);
                foreach (Match match in r.Matches(meta))
                {
                    //得到名字.对多余的换行符进行处理
                    string picname = match.Groups[1].Value.Replace("\n", null);
                    //加入名字
                    XmlElement d = xmldoc.CreateElement("key");
                    d.InnerText = picname;
                    xmlelem.AppendChild(d);
                    //计算x y w h;
                    string x = match.Groups[2].Value;
                    string y = match.Groups[3].Value;
                    string w = match.Groups[4].Value;
                    string h = match.Groups[5].Value;

                    //转换坐标系
                    y = (pic.Height - int.Parse(y) - int.Parse(h)).ToString();

                    //建立内容根
                    d = xmldoc.CreateElement("dict");
                    //加入其它
                    xmladdnode(d, "key", "frame");
                    xmladdnode(d, "string", "{{" + x + "," + y + "},{" + w + "," + h + "}}");
                    xmladdnode(d, "key", "offset");
                    xmladdnode(d, "string", "{0,0}");
                    xmladdnode(d, "key", "rotated");
                    xmladdnode(d, "false", "");
                    xmladdnode(d, "key", "sourceColorRect");
                    xmladdnode(d, "string", "{{" + x + "," + y + "},{" + w + "," + h + "}}");
                    xmladdnode(d, "key", "sourceSize");
                    xmladdnode(d, "string", "{" + w + "," + h + "}");
                    xmlelem.AppendChild(d);
                    if (checkBox_smallpic.Checked)
                        imageCut(pic,
                            int.Parse(x),
                            int.Parse(y),
                            int.Parse(w),
                            int.Parse(h),
                            picname,
                            Path.GetDirectoryName(filePath)
                            );

                }
                if (checkBox_plist.Checked)
                {
                    //写入最后信息
                    root.AppendChild(xmlelem);

                    XmlElement d = xmldoc.CreateElement("key");
                    d.InnerText = "metadata";
                    root.AppendChild(d);
                    d = xmldoc.CreateElement("dict");
                    xmladdnode(root, "key", "format");
                    xmladdnode(root, "integer", "2");
                    xmladdnode(root, "key", "realTextureFileName");
                    xmladdnode(root, "string", Path.GetFileName(picpath));
                    xmladdnode(root, "key", "size");
                    xmladdnode(root, "string", "{" + pic.Width + "," + pic.Height + "}");
                    xmladdnode(root, "key", "TextureFileName");
                    xmladdnode(root, "string", Path.GetFileName(picpath));

                    root.AppendChild(d);
                    string plistpath = Path.ChangeExtension(filePath, null);//去掉meta
                    plistpath = Path.ChangeExtension(plistpath, "plist");//把png改成plist
                    xmldoc.Save(plistpath);
                }
                //关掉文件
                aFile.Close();
                listBox1.Items[0] += "--完成";
            }
        }
        void imageCut(Image i, int x, int y, int w, int h, string name, string dir)
        {
            Bitmap bitmap = new Bitmap(w, h);
            Graphics gp = Graphics.FromImage(bitmap);
            gp.DrawImage(i, new Rectangle(0, 0, w, h), x, y, w, h, GraphicsUnit.Pixel);
            bitmap.Save(dir + "\\" + name + ".png");
        }
        void xmladdnode(XmlNode jroot, string name, string value = null)
        {
            XmlElement p = xmldoc.CreateElement(name);
            if (value != null)
                p.InnerText = value;
            jroot.AppendChild(p);
        }
    }
}
